﻿namespace API.Enumerators
{
    public enum ProfileType
    {
        personal = 0,
        professional = 1,
    }
}
