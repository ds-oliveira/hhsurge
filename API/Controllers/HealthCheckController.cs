using API.Models;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [ApiController]
    public class HealthCheck : Controller
    {
        [HttpGet("/Health")]
        public IActionResult Get() => Json(new MessageResponse("OK"));
    }
}