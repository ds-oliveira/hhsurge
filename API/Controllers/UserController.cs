﻿using System;
using API.Constants;
using API.Errors;
using API.Models;
using API.RequestModels;
using API.Services;
using API.Validations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Text.RegularExpressions;

namespace API.Controllers
{
    [ApiController]
    public class UserController : Controller
    {
        private readonly ILogger<UserController> _logger;
        private readonly UserService _userService;
        private readonly AuthenticationService _authenticationService;
        private static readonly string _emailPattern = @"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}";
        private IActionResult Envelop(Func<IActionResult> method)
        {
            try
            {
                return method();
            }
            catch (EmptyParameterException error)
            {
                _logger.LogError(error, "Request Error");
                return StatusCode(400, new MessageResponse(error.Message));
            }
            catch (UserNotFoundException error)
            {
                _logger.LogError(error, "Request Error");
                return StatusCode(400, new MessageResponse(error.Message));
            }
            catch (InvalidProfileTypeException error)
            {
                _logger.LogError(error, "Request Error");
                return StatusCode(400, new MessageResponse(error.Message));
            }
            catch (InvalidParameterFormatException error)
            {
                _logger.LogError(error, "Request Error");
                return StatusCode(400, new MessageResponse(error.Message));
            }
            catch (InvalidParameterSizeException error)
            {
                _logger.LogError(error, "Request Error");
                return StatusCode(400, new MessageResponse(error.Message));
            }
            catch (InvalidAPIKeyException error)
            {
                _logger.LogError(error, "Request Error");
                return StatusCode(403, new MessageResponse(error.Message));
            }
            catch (EmailAlreadyRegisteredException error)
            {
                _logger.LogError(error, "Request Error");
                return StatusCode(409, new MessageResponse(error.Message));
            }
            catch (Exception error)
            {
                _logger.LogError(error, "Unexpected Error");
                return StatusCode(500, new MessageResponse("Unexpected error."));
            }
        }

        public UserController(ILogger<UserController> logger, UserService userService, AuthenticationService authenticationService)
        {
            _logger = logger;
            _userService = userService;
            _authenticationService = authenticationService;
        }

        [HttpGet("Users/")]
        public IActionResult Get(string apiKey, int limit, int offset) => Envelop(() =>
            {
                _authenticationService.Validate(apiKey);
                return Json(_userService.Read(limit, offset));
            }
        );

        [HttpGet("[controller]")]
        public IActionResult Get(string apiKey, string email) => Envelop(() =>
            {
                _authenticationService.Validate(apiKey);

                if (string.IsNullOrWhiteSpace(email))
                {
                    throw new EmptyParameterException("email");
                }

                if (!Regex.IsMatch(email, _emailPattern))
                {
                    throw new InvalidParameterFormatException("email", _emailPattern);
                }

                return Json(_userService.Read(email));
            });

        [HttpPost("[controller]/")]
        public IActionResult Post(UserRequestModel userRequestModel) => Envelop(() =>
         {
             _authenticationService.Validate(userRequestModel.APIKey);

             RequestModelsValidations.Validate(userRequestModel);
             var user = userRequestModel.MapToUserModel();
             _userService.Create(user);

             return Json(new MessageResponse(SuccessfulMessages.USER_INSERTED));
         });

        [HttpPut("[controller]/")]
        public IActionResult Put(UserRequestModel userRequestModel) => Envelop(() =>
        {
            _authenticationService.Validate(userRequestModel.APIKey);

            RequestModelsValidations.Validate(userRequestModel);
            var user = userRequestModel.MapToUserModel();
            _userService.Update(user);

            return Json(new MessageResponse(SuccessfulMessages.USER_UPDATED));
        });

        [HttpDelete("[controller]/")]
        public IActionResult Delete(UserDeleteRequestModel userRequestModel) => Envelop(() =>
        {
            _authenticationService.Validate(userRequestModel.APIKey);

            RequestModelsValidations.Validate(userRequestModel);
            _userService.Delete(userRequestModel.Email);

            return Json(new MessageResponse(SuccessfulMessages.USER_DELETED));
        });
    }
}
