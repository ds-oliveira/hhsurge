using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using API.SettingsInjection;
using Microsoft.Extensions.Options;
using API.Repositories;
using API.Services;

namespace API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // App Settings
            services.Configure<MongoSettings>(Configuration.GetSection(nameof(MongoSettings)));
            services.Configure<APIKeySettings>(Configuration.GetSection(nameof(APIKeySettings)));

            // Dependency injection
            services.AddSingleton<IMongoSettings>(sp =>sp.GetRequiredService<IOptions<MongoSettings>>().Value);
            services.AddSingleton<IAPIKeySettings>(sp => sp.GetRequiredService<IOptions<APIKeySettings>>().Value);
            services.AddSingleton<MongoDBClient>();
            services.AddSingleton<UserService>();
            services.AddSingleton<AuthenticationService>();

            // Controllers
            services.AddControllers();

            // Documentation
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1",
                    new OpenApiInfo
                    {
                        Title = "Users API",
                        Version = "v1",
                        Description = "API desenvolvida para fins de avaliação.",
                        Contact = new OpenApiContact
                        {
                            Name = "Danilo Oliveira",
                            Url = new Uri("https://github.com/ds-oliveira")
                        }
                    });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            // Activating middlewares for Swagger
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Users API V1");
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
