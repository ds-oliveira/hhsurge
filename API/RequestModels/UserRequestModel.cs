﻿using API.Interfaces;
using API.Models;

namespace API.RequestModels
{
    public class UserRequestModel : ISecureRequest
    {
        public string APIKey { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public ProfileRequestModel Profile { get; set; }

        public User MapToUserModel()
        {
            var address = new Address(
                      Profile.Address.Address1,
                      Profile.Address.Address2,
                      Profile.Address.Zipcode,
                      Profile.Address.City,
                      Profile.Address.State,
                      Profile.Address.Country
                  );

            var profile = new Profile(
                    Profile.Type,
                    Profile.Avatar,
                    Profile.Document,
                    address
                );

            return new User(Name, Email, profile);
        }
    }
}