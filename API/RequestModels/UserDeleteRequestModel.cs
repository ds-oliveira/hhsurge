﻿using API.Interfaces;

namespace API.RequestModels
{
    public class UserDeleteRequestModel : ISecureRequest
    {
        public string APIKey { get; set; }

        public string Email { get; set; }
    }
}