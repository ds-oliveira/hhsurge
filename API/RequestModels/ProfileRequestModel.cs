﻿namespace API.RequestModels
{
    public class ProfileRequestModel
    {
        public string Type { get; set; }

        public string Avatar { get; set; }

        public string Document { get; set; }

        public AddressRequestModel Address { get; set; }
    }
}
