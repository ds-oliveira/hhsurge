﻿using API.Models;
using API.SettingsInjection;
using MongoDB.Driver;

namespace API.Repositories
{
    public class MongoDBClient
    {

        private readonly IMongoDatabase _database;
        private readonly IMongoCollection<User> _userColletion;

        public IMongoDatabase Database
        {
            get { return _database; }
        }

        public IMongoCollection<User> UserCollection
        {
            get { return _userColletion; }
        }

        public MongoDBClient(IMongoSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            _database = client.GetDatabase(settings.DatabaseName);
            _userColletion = _database.GetCollection<User>(settings.UserCollectionName);
        }
    }
}
