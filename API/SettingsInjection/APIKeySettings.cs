﻿namespace API.SettingsInjection
{
    public class APIKeySettings : IAPIKeySettings
    {
        public string APIKey { get; set; }
    }

    public interface IAPIKeySettings
    {
        string APIKey { get; set; }
    }
}
