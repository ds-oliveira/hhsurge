﻿using System;
using System.Collections.Generic;
using System.Linq;
using API.Enumerators;
using API.Errors;
using API.RequestModels;
using System.Text.RegularExpressions;

namespace API.Validations
{
    public class RequestModelsValidations
    {
        private static readonly IEnumerable<string> _allowedTypes = Enum.GetNames(typeof(ProfileType));
        private static readonly string _emailPattern = @"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}";
        private static readonly string _onlyNumbers = @"^\d+$";
        private static readonly string _onlyLetters = @"^[A-Za-z\s]+$";
        private static readonly string _imageUrl = @"(http(s?):)([/|.|\w|\s|-])*\.(?:jpg|gif|png)";

        public static void Validate(AddressRequestModel model)
        {
            model.Address1 = string.IsNullOrWhiteSpace(model.Address1) ? null : model.Address1.Trim();
            model.Address2 = string.IsNullOrWhiteSpace(model.Address2) ? null : model.Address2.Trim();
            model.Zipcode = string.IsNullOrWhiteSpace(model.Zipcode) ? null : model.Zipcode.Trim();
            model.City = string.IsNullOrWhiteSpace(model.City) ? null : model.City.Trim();
            model.State = string.IsNullOrWhiteSpace(model.State) ? null : model.State.Trim();
            model.Country = string.IsNullOrWhiteSpace(model.Country) ? null : model.Country.Trim();

            if (string.IsNullOrWhiteSpace(model.Address1))
            {
                throw new EmptyParameterException("address1");
            }

            if (model.Address1.Length < 3 || model.Address1.Length > 100)
            {
                throw new InvalidParameterSizeException("address1", 3, 100);
            }

            if (!string.IsNullOrWhiteSpace(model.Address2) && (model.Address2.Length < 3 || model.Address2.Length > 100))
            {
                throw new InvalidParameterSizeException("address2", 3, 100);
            }

            if (string.IsNullOrWhiteSpace(model.City))
            {
                throw new EmptyParameterException("city");
            }

            if (model.City.Length < 3 || model.City.Length > 50)
            {
                throw new InvalidParameterSizeException("city", 3, 50);
            }

            if (!Regex.IsMatch(model.City, _onlyLetters))
            {
                throw new InvalidParameterFormatException("city", _onlyLetters);
            }

            if (string.IsNullOrWhiteSpace(model.Country))
            {
                throw new EmptyParameterException("country");
            }

            if (model.Country.Length < 3 || model.Country.Length > 50)
            {
                throw new InvalidParameterSizeException("country", 3, 50);
            }

            if (!Regex.IsMatch(model.Country, _onlyLetters))
            {
                throw new InvalidParameterFormatException("country", _onlyLetters);
            }

            if (string.IsNullOrWhiteSpace(model.State))
            {
                throw new EmptyParameterException("state");
            }

            if (model.State.Length < 2 || model.State.Length > 50)
            {
                throw new InvalidParameterSizeException("state", 2, 50);
            }

            if (!Regex.IsMatch(model.State, _onlyLetters))
            {
                throw new InvalidParameterFormatException("state", _onlyLetters);
            }

            if (string.IsNullOrWhiteSpace(model.Zipcode))
            {
                throw new EmptyParameterException("zipcode");
            }

            if (model.Zipcode.Length < 5 || model.Zipcode.Length > 12)
            {
                throw new InvalidParameterSizeException("zipcode", 5, 12);
            }

            if (!Regex.IsMatch(model.Zipcode, _onlyNumbers))
            {
                throw new InvalidParameterFormatException("zipcode", _onlyNumbers);
            }
        }

        public static void Validate(ProfileRequestModel model)
        {
            model.Type = string.IsNullOrWhiteSpace(model.Type) ? null : model.Type.Trim();
            model.Avatar = string.IsNullOrWhiteSpace(model.Avatar) ? null : model.Avatar.Trim();
            model.Document = string.IsNullOrWhiteSpace(model.Document) ? null : model.Document.Trim();

            if (!_allowedTypes.Contains(model.Type))
            {
                throw new InvalidProfileTypeException();
            }

            if (string.IsNullOrWhiteSpace(model.Avatar))
            {
                throw new EmptyParameterException("avatar");
            }

            if (!Regex.IsMatch(model.Avatar, _imageUrl))
            {
                throw new InvalidParameterFormatException("avatar", _imageUrl);
            }

            if (string.IsNullOrWhiteSpace(model.Document))
            {
                throw new EmptyParameterException("document");
            }

            if (model.Document.Length < 8 || model.Document.Length > 15)
            {
                throw new InvalidParameterSizeException("document", 8, 15);
            }

            if (!Regex.IsMatch(model.Document, _onlyNumbers))
            {
                throw new InvalidParameterFormatException("document", _onlyNumbers);
            }

            if (model.Address == null)
            {
                throw new EmptyParameterException("address");
            }

            Validate(model.Address);
        }

        public static void Validate(UserDeleteRequestModel model)
        {
            model.Email = string.IsNullOrWhiteSpace(model.Email) ? null : model.Email.Trim();

            if (string.IsNullOrWhiteSpace(model.Email))
            {
                throw new EmptyParameterException("email");
            }

            if (!Regex.IsMatch(model.Email, _emailPattern))
            {
                throw new InvalidParameterFormatException("email", _emailPattern);
            }
        }

        public static void Validate(UserRequestModel model)
        {
            model.Email = string.IsNullOrWhiteSpace(model.Email) ? null : model.Email.Trim();
            model.Name = string.IsNullOrWhiteSpace(model.Name) ? null : model.Name.Trim();

            if (string.IsNullOrWhiteSpace(model.Name))
            {
                throw new EmptyParameterException("name");
            }

            if (model.Name.Length < 3 || model.Name.Length > 100)
            {
                throw new InvalidParameterSizeException("name", 3, 100);
            }

            if (!Regex.IsMatch(model.Name, _onlyLetters))
            {
                throw new InvalidParameterFormatException("name", _onlyLetters);
            }

            if (string.IsNullOrWhiteSpace(model.Email))
            {
                throw new EmptyParameterException("email");
            }

            if (!Regex.IsMatch(model.Email, _emailPattern))
            {
                throw new InvalidParameterFormatException("email", _emailPattern);
            }

            if (model.Profile == null)
            {
                throw new EmptyParameterException("profile");
            }

            Validate(model.Profile);
        }
    }
}
