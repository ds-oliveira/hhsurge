﻿namespace API.Constants
{
    public static class SuccessfulMessages
    {
        public static readonly string USER_UPDATED = "User updated.";
        public static readonly string USER_INSERTED = "User inserted.";
        public static readonly string USER_DELETED = "User deleted.";
    }
}
