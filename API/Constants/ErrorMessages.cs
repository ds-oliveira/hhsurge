﻿namespace API.Constants
{
    public static class ErrorMessages
    {
        public static readonly string EMAIL_ALREADY_REGISTERED = "The given email is already registered for another account.";
        public static readonly string PARAMETER_NOT_INFORMED = "A required parameter was not informed: {0}";
        public static readonly string INVALID_PROFILE_TYPE = "The profile type is not valid, please choose between 'personal' and 'professional'.";
        public static readonly string USER_NOT_FOUND = "There aren't users with the given email.";
        public static readonly string INVALID_API_KEY = "Invalid API Key.";
        public static readonly string INVALID_LENGTH = "The param '{0}' size must be between {1} and {2} characters.";
        public static readonly string INVALID_PATTERN = "The param '{0}' pattern must match the following: '{1}'.";
    }
}
