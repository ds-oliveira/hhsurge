﻿using System.Collections.Generic;

namespace API.Interfaces
{
    public interface ICRUD<T>
    {
        public void Create(T model);

        public void Update(T model);

        public void Delete(string identifier);

        public List<T> Read(int limit, int offset);

        public T Read(string identifier);
    }
}
