﻿namespace API.Interfaces
{
    public interface ISecureRequest
    {
        public string APIKey { get; set; }
    }
}
