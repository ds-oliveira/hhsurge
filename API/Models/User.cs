﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace API.Models
{
    public class User
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id{ get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public Profile Profile { get; set; }

        public User(string name, string email, Profile profile)
        {
            Name = name;
            Email = email;
            Profile = profile;
        }
    }
}
