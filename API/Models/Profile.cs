﻿namespace API.Models
{
    public class Profile
    {
        public string Id { get; set; }

        public string Type { get; set; }

        public string Avatar { get; set; }

        public string Document { get; set; }

        public Address Address { get; set; }

        public Profile(string type, string avatar, string document, Address address)
        {
            Type = type;
            Avatar = avatar;
            Document = document;
            Address = address;
        }
    }
}
