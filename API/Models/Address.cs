﻿namespace API.Models
{
    public class Address
    {
        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Zipcode { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Country { get; set; }

        public Address(string address1, string address2, string zipcode, string city, string state, string country)
        {
            Address1 = address1;
            Address2 = address2;
            Zipcode = zipcode;
            City = city;
            State = state;
            Country = country;
        }
    }
}
