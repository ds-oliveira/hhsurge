﻿using System;
using API.Constants;

namespace API.Errors
{
    public class InvalidProfileTypeException : Exception
    {
        public override string Message
        {
            get
            {
                return ErrorMessages.INVALID_PROFILE_TYPE;
            }
        }
    }
}
