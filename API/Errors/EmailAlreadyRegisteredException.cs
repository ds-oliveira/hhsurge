﻿using System;
using API.Constants;

namespace API.Errors
{
    public class EmailAlreadyRegisteredException : Exception
    {
        public override string Message
        {
            get
            {
                return ErrorMessages.EMAIL_ALREADY_REGISTERED;
            }
        }
    }
}