﻿using System;
using API.Constants;

namespace API.Errors
{
    public class InvalidParameterSizeException : Exception
    {
        public InvalidParameterSizeException(string param, int min, int max) : base(string.Format(ErrorMessages.INVALID_LENGTH, param, min, max)) { }
    }
}
