﻿using System;
using API.Constants;

namespace API.Errors
{
    public class InvalidAPIKeyException : Exception
    {
        public override string Message
        {
            get
            {
                return ErrorMessages.INVALID_API_KEY;
            }
        }
    }
}
