﻿using System;
using API.Constants;

namespace API.Errors
{
    public class InvalidParameterFormatException : Exception
    {
        public InvalidParameterFormatException(string param, string pattern) : base(string.Format(ErrorMessages.INVALID_PATTERN, param, pattern)) { }
    }
}
