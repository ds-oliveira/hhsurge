﻿using System;
using API.Constants;

namespace API.Errors
{
    public class EmptyParameterException : Exception
    {
        public EmptyParameterException(string param) : base(string.Format(ErrorMessages.PARAMETER_NOT_INFORMED, param)) { }
    }
}
