﻿using System;
using API.Constants;

namespace API.Errors
{
    public class UserNotFoundException : Exception
    {
        public override string Message
        {
            get
            {
                return ErrorMessages.USER_NOT_FOUND;
            }
        }
    }
}
