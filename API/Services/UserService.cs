﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using API.Errors;
using API.Interfaces;
using API.Models;
using API.Repositories;
using MongoDB.Driver;

namespace API.Services
{
    public class UserService : ICRUD<User>
    {
        private readonly MongoDBClient _mongoDBClient;
        private readonly MD5 md5Hash = MD5.Create();

        public UserService(MongoDBClient mongoDBClient)
        {
            _mongoDBClient = mongoDBClient;
        }

        public void Create(User user)
        {
            var registeredUser = _mongoDBClient.UserCollection.Find(registeredUser => registeredUser.Email == user.Email).FirstOrDefault();

            if (registeredUser != null)
            {
                throw new EmailAlreadyRegisteredException();
            }

            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(user.Email));
            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            user.Profile.Id = sBuilder.ToString();
            _mongoDBClient.UserCollection.InsertOne(user);
        }

        public void Delete(string identifier)
        {
            var currentData = _mongoDBClient.UserCollection.Find(current => current.Email == identifier).FirstOrDefault();

            if (currentData == null)
            {
                throw new UserNotFoundException();
            }


            _mongoDBClient.UserCollection.DeleteOne(user => user.Email == identifier);
        }

        public List<User> Read(int limit, int offset)
        {
            if (limit == 0)
            {
                return new List<User>();
            }

            return _mongoDBClient.UserCollection.Find(user => true).Skip(offset).Limit(limit).ToList();
        }

        public User Read(string identifier)
        {
            var user = _mongoDBClient.UserCollection.Find(user => user.Email == identifier).FirstOrDefault();

            if (user == null)
            {
                throw new UserNotFoundException();
            }

            return user;
        }

        public void Update(User user)
        {
            var currentData = _mongoDBClient.UserCollection.Find(current => current.Email == user.Email).FirstOrDefault();

            if (currentData == null)
            {
                throw new UserNotFoundException();
            }

            user.Id = currentData.Id;
            user.Profile.Id = currentData.Profile.Id;
            _mongoDBClient.UserCollection.ReplaceOne(originalUser => originalUser.Email == user.Email, user);
        }
    }
}
