﻿using API.Errors;
using API.SettingsInjection;

namespace API.Services
{
    public class AuthenticationService
    {
        private string _apiKey;

        public AuthenticationService(IAPIKeySettings settings)
        {
            _apiKey = settings.APIKey;
        }

        public void Validate(string token) {
            if (string.IsNullOrWhiteSpace(token) || !token.Equals(_apiKey))
            {
                throw new InvalidAPIKeyException();
            }
        }
    }
}
