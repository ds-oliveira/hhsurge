﻿using System;
using API.Enumerators;
using NUnit.Framework;

namespace Tests.Enumerators
{
    public class ProfileTypeTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void CreateAddress()
        {
            Assert.AreEqual(0, Convert.ToInt32(ProfileType.personal));
            Assert.AreEqual(1, Convert.ToInt32(ProfileType.professional));
            Assert.AreEqual("personal", ProfileType.personal.ToString());
            Assert.AreEqual("professional",ProfileType.professional.ToString());
        }
    }
}
