﻿using NUnit.Framework;
using API.Services;
using System;
using API.SettingsInjection;

namespace API.Tests
{
    public class AuthenticationServiceTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void AuthenticateWithSuccess()
        {
            var settings = new APIKeySettings();
            settings.APIKey = "test-key";

            var service = new AuthenticationService(settings);
            service.Validate("test-key");
            Assert.Pass();
        }

        [Test]
        public void AunthenticationFail()
        {
            var settings = new APIKeySettings();
            settings.APIKey = "test-key";

            var service = new AuthenticationService(settings);

            try
            {
                service.Validate("test-key1");
            }
            catch (Exception error)
            {
                Assert.AreEqual("Invalid API Key.", error.Message);
            }
        }
    }
}
