﻿using NUnit.Framework;
using API.SettingsInjection;
using System.Linq;

namespace API.Tests
{
    public class APIKeySettingsTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void GetSetAPIKeySettings()
        {
            var obj = new APIKeySettings();
            obj.APIKey = "api-key";
            Assert.AreEqual(obj.APIKey, "api-key");
        }

        [Test]
        public void VerifyAPIKeySettingProperties()
        {
            var properties = typeof(IAPIKeySettings).GetProperties().ToList().Select(x => x.Name).ToArray();
            Assert.AreEqual(1, properties.Length);
            Assert.Contains("APIKey", properties);
        }
    }
}
