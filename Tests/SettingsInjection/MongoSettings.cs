﻿using NUnit.Framework;
using API.SettingsInjection;
using System.Linq;

namespace API.Tests
{
    public class MongoSettingsTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void GetSetMongoSettings()
        {
            var obj = new MongoSettings();
            obj.ConnectionString = "connection";
            obj.DatabaseName = "base-test";
            obj.UserCollectionName = "users";

            Assert.AreEqual(obj.ConnectionString, "connection");
            Assert.AreEqual(obj.DatabaseName, "base-test");
            Assert.AreEqual(obj.UserCollectionName, "users");
        }

        [Test]
        public void VerifyMongoSettingProperties()
        {
            var properties = typeof(IMongoSettings).GetProperties().ToList().Select(x => x.Name).ToArray();
            Assert.AreEqual(3, properties.Length);
            Assert.Contains("ConnectionString", properties);
            Assert.Contains("DatabaseName", properties);
            Assert.Contains("UserCollectionName", properties);
        }
    }
}
