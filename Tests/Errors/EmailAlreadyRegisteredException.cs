﻿using API.Errors;
using NUnit.Framework;

namespace Tests.Enumerators
{
    public class EmailAlreadyRegisteredExceptionTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void EmailAlreadyRegisteredExceptionMessage()
        {
            try
            {
                throw new EmailAlreadyRegisteredException();
            }
            catch (EmailAlreadyRegisteredException error)
            {
                Assert.AreEqual(error.Message, "The given email is already registered for another account.");
            }
        }
    }
}
