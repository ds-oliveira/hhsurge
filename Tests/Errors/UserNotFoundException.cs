﻿using API.Errors;
using NUnit.Framework;

namespace Tests.Enumerators
{
    public class UserNotFoundExceptionTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void UserNotFoundExceptionMessage()
        {
            try
            {
                throw new UserNotFoundException();
            }
            catch (UserNotFoundException error)
            {
                Assert.AreEqual(error.Message, "There aren't users with the given email.");
            }
        }
    }
}
