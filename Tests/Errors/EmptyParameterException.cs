﻿using API.Errors;
using NUnit.Framework;

namespace Tests.Enumerators
{
    public class EmptyParameterExceptionTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void EmptyParameterExceptionMessage()
        {
            try
            {
                throw new EmptyParameterException("test");
            }
            catch (EmptyParameterException error)
            {
                Assert.AreEqual(error.Message, "A required parameter was not informed: test");
            }
        }
    }
}
