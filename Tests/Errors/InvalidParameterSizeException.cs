﻿using API.Errors;
using NUnit.Framework;

namespace Tests.Enumerators
{
    public class InvalidParameterSizeExceptionTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void InvalidParameterSizeExceptionMessage()
        {
            try
            {
                throw new InvalidParameterSizeException("test", 1, 2);
            }
            catch (InvalidParameterSizeException error)
            {
                Assert.AreEqual(error.Message, "The param 'test' size must be between 1 and 2 characters.");
            }
        }
    }
}
