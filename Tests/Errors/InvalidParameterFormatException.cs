﻿using API.Errors;
using NUnit.Framework;

namespace Tests.Enumerators
{
    public class InvalidParameterFormatExceptionTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void InvalidParameterFormatExceptionMessage()
        {
            try
            {
                throw new InvalidParameterFormatException("test", "pattern");
            }
            catch (InvalidParameterFormatException error)
            {
                Assert.AreEqual(error.Message, "The param 'test' pattern must match the following: 'pattern'.");
            }
        }
    }
}
