﻿using API.Errors;
using NUnit.Framework;

namespace Tests.Enumerators
{
    public class InvalidAPIKeyExceptionTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void InvalidAPIKeyExceptionMessage()
        {
            try
            {
                throw new InvalidAPIKeyException();
            }
            catch (InvalidAPIKeyException error)
            {
                Assert.AreEqual(error.Message, "Invalid API Key.");
            }
        }
    }
}
