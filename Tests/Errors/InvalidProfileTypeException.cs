﻿using API.Errors;
using NUnit.Framework;

namespace Tests.Enumerators
{
    public class InvalidProfileTypeExceptionTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void InvalidProfileTypeExceptionMessage()
        {
            try
            {
                throw new InvalidProfileTypeException();
            }
            catch (InvalidProfileTypeException error)
            {
                Assert.AreEqual(error.Message, "The profile type is not valid, please choose between 'personal' and 'professional'.");
            }
        }
    }
}
