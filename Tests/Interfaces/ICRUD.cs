using NUnit.Framework;
using API.Interfaces;
using System.Linq;

namespace API.Tests
{
    public class ICRUDTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void VerifyICRUDMethods()
        {
            var methods = typeof(ICRUD<object>).GetMethods().ToList().Select(x => x.Name).ToArray();
            Assert.AreEqual(5, methods.Length);
            Assert.Contains("Create", methods);
            Assert.Contains("Update", methods);
            Assert.Contains("Delete", methods);
            Assert.Contains("Read", methods);
        }
    }
}