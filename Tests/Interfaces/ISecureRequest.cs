using NUnit.Framework;
using API.Interfaces;
using System.Linq;

namespace API.Tests
{
    public class ISecureRequestTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void VerifyISecureRequestProperties()
        {
            var properties = typeof(ISecureRequest).GetProperties().ToList().Select(x => x.Name).ToArray();
            Assert.AreEqual(1, properties.Length);
            Assert.Contains("APIKey", properties);
        }
    }
}