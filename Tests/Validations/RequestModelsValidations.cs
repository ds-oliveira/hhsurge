using NUnit.Framework;
using API.RequestModels;
using API.Validations;
using System;

namespace API.Tests
{
    public class RequestModelsValidationsTest
    {
        private UserRequestModel _validUserRequestModel;

        private static readonly string _emailPattern = @"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}";
        private static readonly string _onlyNumbers = @"^\d+$";
        private static readonly string _onlyLetters = @"^[A-Za-z\s]+$";
        private static readonly string _imageUrl = @"(http(s?):)([/|.|\w|\s|-])*\.(?:jpg|gif|png)";

        [SetUp]
        public void Setup()
        {
            var address = new AddressRequestModel();
            address.Address1 = "Rua itapeva, 164";
            address.Address2 = "Apartamento 145";
            address.City = "Sao Paulo";
            address.Country = "Brazil";
            address.State = "SP";
            address.Zipcode = "01332000";

            var profile = new ProfileRequestModel();
            profile.Address = address;
            profile.Avatar = "https://img2.gratispng.com/20180408/tvw/kisspng-user-computer-icons-gravatar-blog-happy-woman-5aca6d03e6c3f5.6041125615232156199452.jpg";
            profile.Document = "483051318";
            profile.Type = "personal";

            _validUserRequestModel = new UserRequestModel();
            _validUserRequestModel.APIKey = "test-key";
            _validUserRequestModel.Email = "dsoliveira.developer@gmail.com";
            _validUserRequestModel.Name = "Danilo Oliveira";
            _validUserRequestModel.Profile = profile;
        }

        [Test]
        public void UserRequestModelNameValidationTest()
        {
            var name = _validUserRequestModel.Name;

            _validUserRequestModel.Name = null;
            try
            {
                RequestModelsValidations.Validate(_validUserRequestModel);
                Assert.Fail();
            }
            catch (Exception error)
            {
                Assert.AreEqual(error.Message, "A required parameter was not informed: name");
            }

            _validUserRequestModel.Name = "d";
            try
            {
                RequestModelsValidations.Validate(_validUserRequestModel);
                Assert.Fail();
            }
            catch (Exception error)
            {
                Assert.AreEqual(error.Message, "The param 'name' size must be between 3 and 100 characters.");
            }

            _validUserRequestModel.Name = "danilo1";
            try
            {
                RequestModelsValidations.Validate(_validUserRequestModel);
                Assert.Fail();
            }
            catch (Exception error)
            {
                Assert.AreEqual(error.Message, "The param 'name' pattern must match the following: '" + _onlyLetters + "'.");
            }

            _validUserRequestModel.Name = name;
            RequestModelsValidations.Validate(_validUserRequestModel);
        }

        [Test]
        public void UserRequestModelEmailValidationTest()
        {
            var email = _validUserRequestModel.Email;

            _validUserRequestModel.Email = null;
            try
            {
                RequestModelsValidations.Validate(_validUserRequestModel);
                Assert.Fail();
            }
            catch (Exception error)
            {
                Assert.AreEqual(error.Message, "A required parameter was not informed: email");
            }

            _validUserRequestModel.Email = "d";
            try
            {
                RequestModelsValidations.Validate(_validUserRequestModel);
                Assert.Fail();
            }
            catch (Exception error)
            {
                Assert.AreEqual(error.Message, "The param 'email' pattern must match the following: '" + _emailPattern + "'.");
            }

            _validUserRequestModel.Email = email;
            RequestModelsValidations.Validate(_validUserRequestModel);
        }

        [Test]
        public void UserRequestModelProfileValidationTest()
        {
            var profile = _validUserRequestModel.Profile;

            _validUserRequestModel.Profile = null;
            try
            {
                RequestModelsValidations.Validate(_validUserRequestModel);
                Assert.Fail();
            }
            catch (Exception error)
            {
                Assert.AreEqual(error.Message, "A required parameter was not informed: profile");
            }

            _validUserRequestModel.Profile = profile;
            RequestModelsValidations.Validate(_validUserRequestModel);
        }

        [Test]
        public void UserRequestModelAvatarValidationTest()
        {
            var avatar = _validUserRequestModel.Profile.Avatar;

            _validUserRequestModel.Profile.Avatar = null;
            try
            {
                RequestModelsValidations.Validate(_validUserRequestModel);
                Assert.Fail();
            }
            catch (Exception error)
            {
                Assert.AreEqual(error.Message, "A required parameter was not informed: avatar");
            }

            _validUserRequestModel.Profile.Avatar = "dsadas";
            try
            {
                RequestModelsValidations.Validate(_validUserRequestModel);
                Assert.Fail();
            }
            catch (Exception error)
            {
                Assert.AreEqual(error.Message, "The param 'avatar' pattern must match the following: '" + _imageUrl + "'.");
            }

            _validUserRequestModel.Profile.Avatar = avatar;
            RequestModelsValidations.Validate(_validUserRequestModel);
        }

        [Test]
        public void UserRequestModelDocumentValidationTest()
        {
            var document = _validUserRequestModel.Profile.Document;

            _validUserRequestModel.Profile.Document = null;
            try
            {
                RequestModelsValidations.Validate(_validUserRequestModel);
                Assert.Fail();
            }
            catch (Exception error)
            {
                Assert.AreEqual(error.Message, "A required parameter was not informed: document");
            }

            _validUserRequestModel.Profile.Document = "123";
            try
            {
                RequestModelsValidations.Validate(_validUserRequestModel);
                Assert.Fail();
            }
            catch (Exception error)
            {
                Assert.AreEqual(error.Message, "The param 'document' size must be between 8 and 15 characters.");
            }

            _validUserRequestModel.Profile.Document = "3123123213d";
            try
            {
                RequestModelsValidations.Validate(_validUserRequestModel);
                Assert.Fail();
            }
            catch (Exception error)
            {
                Assert.AreEqual(error.Message, "The param 'document' pattern must match the following: '" + _onlyNumbers + "'.");
            }

            _validUserRequestModel.Profile.Document = document;
            RequestModelsValidations.Validate(_validUserRequestModel);
        }

        [Test]
        public void UserRequestModelTypeValidationTest()
        {
            var type = _validUserRequestModel.Profile.Type;

            _validUserRequestModel.Profile.Type = null;
            try
            {
                RequestModelsValidations.Validate(_validUserRequestModel);
                Assert.Fail();
            }
            catch (Exception error)
            {
                Assert.AreEqual(error.Message, "The profile type is not valid, please choose between 'personal' and 'professional'.");
            }

            _validUserRequestModel.Profile.Type = "per";
            try
            {
                RequestModelsValidations.Validate(_validUserRequestModel);
                Assert.Fail();
            }
            catch (Exception error)
            {
                Assert.AreEqual(error.Message, "The profile type is not valid, please choose between 'personal' and 'professional'.");
            }

            _validUserRequestModel.Profile.Type = type;
            RequestModelsValidations.Validate(_validUserRequestModel);
        }

        [Test]
        public void UserRequestModelAddressTest()
        {
            var address = _validUserRequestModel.Profile.Address;

            _validUserRequestModel.Profile.Address = null;
            try
            {
                RequestModelsValidations.Validate(_validUserRequestModel);
                Assert.Fail();
            }
            catch (Exception error)
            {
                Assert.AreEqual(error.Message, "A required parameter was not informed: address");
            }

            _validUserRequestModel.Profile.Address = address;
            RequestModelsValidations.Validate(_validUserRequestModel);
        }

        [Test]
        public void UserRequestModelAddress1ValidationTest()
        {
            var address1 = _validUserRequestModel.Profile.Address.Address1;

            _validUserRequestModel.Profile.Address.Address1 = null;
            try
            {
                RequestModelsValidations.Validate(_validUserRequestModel);
                Assert.Fail();
            }
            catch (Exception error)
            {
                Assert.AreEqual(error.Message, "A required parameter was not informed: address1");
            }

            _validUserRequestModel.Profile.Address.Address1 = "12";
            try
            {
                RequestModelsValidations.Validate(_validUserRequestModel);
                Assert.Fail();
            }
            catch (Exception error)
            {
                Assert.AreEqual(error.Message, "The param 'address1' size must be between 3 and 100 characters.");
            }

            _validUserRequestModel.Profile.Address.Address1 = address1;
            RequestModelsValidations.Validate(_validUserRequestModel);
        }

        [Test]
        public void UserRequestModelAddress2ValidationTest()
        {
            var address2 = _validUserRequestModel.Profile.Address.Address2;

            _validUserRequestModel.Profile.Address.Address2 = "12";
            try
            {
                RequestModelsValidations.Validate(_validUserRequestModel);
                Assert.Fail();
            }
            catch (Exception error)
            {
                Assert.AreEqual(error.Message, "The param 'address2' size must be between 3 and 100 characters.");
            }

            _validUserRequestModel.Profile.Address.Address2 = address2;
            RequestModelsValidations.Validate(_validUserRequestModel);
        }

        [Test]
        public void UserRequestModelCityValidationTest()
        {
            var city = _validUserRequestModel.Profile.Address.City;

            _validUserRequestModel.Profile.Address.City = null;
            try
            {
                RequestModelsValidations.Validate(_validUserRequestModel);
                Assert.Fail();
            }
            catch (Exception error)
            {
                Assert.AreEqual(error.Message, "A required parameter was not informed: city");
            }

            _validUserRequestModel.Profile.Address.City = "12";
            try
            {
                RequestModelsValidations.Validate(_validUserRequestModel);
                Assert.Fail();
            }
            catch (Exception error)
            {
                Assert.AreEqual(error.Message, "The param 'city' size must be between 3 and 50 characters.");
            }

            _validUserRequestModel.Profile.Address.City = "123";
            try
            {
                RequestModelsValidations.Validate(_validUserRequestModel);
                Assert.Fail();
            }
            catch (Exception error)
            {
                Assert.AreEqual(error.Message, "The param 'city' pattern must match the following: '" + _onlyLetters + "'.");
            }

            _validUserRequestModel.Profile.Address.City = city;
            RequestModelsValidations.Validate(_validUserRequestModel);
        }

        [Test]
        public void UserRequestModelCountryValidationTest()
        {
            var country = _validUserRequestModel.Profile.Address.Country;

            _validUserRequestModel.Profile.Address.Country = null;
            try
            {
                RequestModelsValidations.Validate(_validUserRequestModel);
                Assert.Fail();
            }
            catch (Exception error)
            {
                Assert.AreEqual(error.Message, "A required parameter was not informed: country");
            }

            _validUserRequestModel.Profile.Address.Country = "12";
            try
            {
                RequestModelsValidations.Validate(_validUserRequestModel);
                Assert.Fail();
            }
            catch (Exception error)
            {
                Assert.AreEqual(error.Message, "The param 'country' size must be between 3 and 50 characters.");
            }

            _validUserRequestModel.Profile.Address.Country = "123";
            try
            {
                RequestModelsValidations.Validate(_validUserRequestModel);
                Assert.Fail();
            }
            catch (Exception error)
            {
                Assert.AreEqual(error.Message, "The param 'country' pattern must match the following: '" + _onlyLetters + "'.");
            }

            _validUserRequestModel.Profile.Address.Country = country;
            RequestModelsValidations.Validate(_validUserRequestModel);
        }

        [Test]
        public void UserRequestModelStateValidationTest()
        {
            var state = _validUserRequestModel.Profile.Address.State;

            _validUserRequestModel.Profile.Address.State = null;
            try
            {
                RequestModelsValidations.Validate(_validUserRequestModel);
                Assert.Fail();
            }
            catch (Exception error)
            {
                Assert.AreEqual(error.Message, "A required parameter was not informed: state");
            }

            _validUserRequestModel.Profile.Address.State = "1";
            try
            {
                RequestModelsValidations.Validate(_validUserRequestModel);
                Assert.Fail();
            }
            catch (Exception error)
            {
                Assert.AreEqual(error.Message, "The param 'state' size must be between 2 and 50 characters.");
            }

            _validUserRequestModel.Profile.Address.State = "123";
            try
            {
                RequestModelsValidations.Validate(_validUserRequestModel);
                Assert.Fail();
            }
            catch (Exception error)
            {
                Assert.AreEqual(error.Message, "The param 'state' pattern must match the following: '" + _onlyLetters + "'.");
            }

            _validUserRequestModel.Profile.Address.State = state;
            RequestModelsValidations.Validate(_validUserRequestModel);
        }

        [Test]
        public void UserRequestModelZipcodeValidationTest()
        {
            var zipcode = _validUserRequestModel.Profile.Address.Zipcode;

            _validUserRequestModel.Profile.Address.Zipcode = null;
            try
            {
                RequestModelsValidations.Validate(_validUserRequestModel);
                Assert.Fail();
            }
            catch (Exception error)
            {
                Assert.AreEqual(error.Message, "A required parameter was not informed: zipcode");
            }

            _validUserRequestModel.Profile.Address.Zipcode = "3123132131231231231321";
            try
            {
                RequestModelsValidations.Validate(_validUserRequestModel);
                Assert.Fail();
            }
            catch (Exception error)
            {
                Assert.AreEqual(error.Message, "The param 'zipcode' size must be between 5 and 12 characters.");
            }

            _validUserRequestModel.Profile.Address.Zipcode = "123323d";
            try
            {
                RequestModelsValidations.Validate(_validUserRequestModel);
                Assert.Fail();
            }
            catch (Exception error)
            {
                Assert.AreEqual(error.Message, "The param 'zipcode' pattern must match the following: '" + _onlyNumbers + "'.");
            }

            _validUserRequestModel.Profile.Address.Zipcode = zipcode;
            RequestModelsValidations.Validate(_validUserRequestModel);
        }
    }
}
