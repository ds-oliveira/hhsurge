﻿using API.Constants;
using NUnit.Framework;

namespace Tests.Enumerators
{
    public class SuccessFulMessagesTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestSuccessMessages()
        {
            Assert.AreEqual(SuccessfulMessages.USER_DELETED, "User deleted.");
            Assert.AreEqual(SuccessfulMessages.USER_INSERTED, "User inserted.");
            Assert.AreEqual(SuccessfulMessages.USER_UPDATED, "User updated.");
        }
    }
}
