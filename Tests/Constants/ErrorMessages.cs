﻿using API.Constants;
using NUnit.Framework;

namespace Tests.Enumerators
{
    public class ErrorMessagesTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestErrorMessages()
        {
            Assert.AreEqual(ErrorMessages.EMAIL_ALREADY_REGISTERED, "The given email is already registered for another account.");
            Assert.AreEqual(ErrorMessages.PARAMETER_NOT_INFORMED, "A required parameter was not informed: {0}");
            Assert.AreEqual(ErrorMessages.INVALID_PROFILE_TYPE, "The profile type is not valid, please choose between 'personal' and 'professional'.");
            Assert.AreEqual(ErrorMessages.USER_NOT_FOUND, "There aren't users with the given email.");
            Assert.AreEqual(ErrorMessages.INVALID_API_KEY, "Invalid API Key.");
            Assert.AreEqual(ErrorMessages.INVALID_LENGTH, "The param '{0}' size must be between {1} and {2} characters.");
            Assert.AreEqual(ErrorMessages.INVALID_PATTERN, "The param '{0}' pattern must match the following: '{1}'.");
        }

        [Test]
        public void TestErrorMessagesFormat()
        {
            Assert.AreEqual(string.Format(ErrorMessages.PARAMETER_NOT_INFORMED, "test"), "A required parameter was not informed: test");
            Assert.AreEqual(string.Format(ErrorMessages.INVALID_LENGTH, "test", 1, 2), "The param 'test' size must be between 1 and 2 characters.");
            Assert.AreEqual(string.Format(ErrorMessages.INVALID_PATTERN, "test", "pattern"), "The param 'test' pattern must match the following: 'pattern'.");
        }
    }
}
