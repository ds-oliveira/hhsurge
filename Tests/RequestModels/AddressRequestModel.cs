using NUnit.Framework;
using API.RequestModels;

namespace API.Tests
{
    public class AddressRequestModelTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void GetSetAddressRequestModelTest()
        {
            var obj = new AddressRequestModel();
            obj.Address1 = "Rua itapeva, 164";
            obj.Address2 = "Apartamento 145";
            obj.City = "Sao Paulo";
            obj.Country = "Brazil";
            obj.State = "SP";
            obj.Zipcode = "01332000";

            Assert.AreEqual(obj.Address1, "Rua itapeva, 164");
            Assert.AreEqual(obj.Address2, "Apartamento 145");
            Assert.AreEqual(obj.City, "Sao Paulo");
            Assert.AreEqual(obj.Country, "Brazil");
            Assert.AreEqual(obj.State, "SP");
            Assert.AreEqual(obj.Zipcode, "01332000");
        }
    }
}