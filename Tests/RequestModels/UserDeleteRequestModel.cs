using NUnit.Framework;
using API.RequestModels;

namespace API.Tests
{
    public class UserDeleteRequestModelTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void GetSetUserDeleteRequestModelTest()
        {
            var obj = new UserDeleteRequestModel();
            obj.APIKey = "api-key";
            obj.Email = "dsoliveira@gmail.com";

            Assert.AreEqual(obj.APIKey, "api-key");
            Assert.AreEqual(obj.Email, "dsoliveira@gmail.com");
        }
    }
}