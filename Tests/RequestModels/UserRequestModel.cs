using NUnit.Framework;
using API.RequestModels;

namespace API.Tests
{
    public class UserRequestModelTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void GetSetUserRequestModelTest()
        {
            var address = new AddressRequestModel();
            address.Address1 = "Rua itapeva, 164";
            address.Address2 = "Apartamento 145";
            address.City = "Sao Paulo";
            address.Country = "Brazil";
            address.State = "SP";
            address.Zipcode = "01332000";

            var profile = new ProfileRequestModel();
            profile.Address = address;
            profile.Avatar = "teste";
            profile.Document = "483051334";
            profile.Type = "personal";

            var user = new UserRequestModel();
            user.APIKey = "test-key";
            user.Email = "dsoliveira@gmail.com";
            user.Name = "Danilo Oliveira";
            user.Profile = profile;

            Assert.AreEqual(user.APIKey, "test-key");
            Assert.AreEqual(user.Email, "dsoliveira@gmail.com");
            Assert.AreEqual(user.Name, "Danilo Oliveira");
            Assert.AreEqual(profile.Avatar, "teste");
            Assert.AreEqual(profile.Document, "483051334");
            Assert.AreEqual(profile.Type, "personal");
            Assert.AreEqual(profile.Address.Address1, "Rua itapeva, 164");
            Assert.AreEqual(profile.Address.Address2, "Apartamento 145");
            Assert.AreEqual(profile.Address.City, "Sao Paulo");
            Assert.AreEqual(profile.Address.Country, "Brazil");
            Assert.AreEqual(profile.Address.State, "SP");
            Assert.AreEqual(profile.Address.Zipcode, "01332000");
        }
    }
}