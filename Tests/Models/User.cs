using NUnit.Framework;
using API.Models;

namespace API.Tests
{
    public class UserTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void CreateUser()
        {
            Address address = new Address("Rua Itapeva, 164", "Apartamento 145", "01332000", "Sao Paulo", "SP", "Brazil");
            Profile profile = new Profile(Enumerators.ProfileType.personal.ToString(), "http://teste.jpg", "32495544895", address);
            User user = new User("Danilo Silva de Oliveira", "dsoliveira.developer@gmail.com", profile);
            Assert.AreEqual("Danilo Silva de Oliveira", user.Name);
            Assert.AreEqual("dsoliveira.developer@gmail.com", user.Email);
            Assert.AreEqual(Enumerators.ProfileType.personal.ToString(), user.Profile.Type);
            Assert.AreEqual("http://teste.jpg", user.Profile.Avatar);
            Assert.AreEqual("32495544895", user.Profile.Document);
            Assert.AreEqual("Rua Itapeva, 164", user.Profile.Address.Address1);
            Assert.AreEqual("Apartamento 145", user.Profile.Address.Address2);
            Assert.AreEqual("01332000", user.Profile.Address.Zipcode);
            Assert.AreEqual("Sao Paulo", user.Profile.Address.City);
            Assert.AreEqual("SP", user.Profile.Address.State);
            Assert.AreEqual("Brazil", user.Profile.Address.Country);
        }
    }
}