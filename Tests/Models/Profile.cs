using NUnit.Framework;
using API.Models;

namespace API.Tests
{
    public class ProfileTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void CreateProfile()
        {
            Address address = new Address("Rua Itapeva, 164", "Apartamento 145", "01332000", "Sao Paulo", "SP", "Brazil");
            Profile profile = new Profile(Enumerators.ProfileType.personal.ToString(), "http://teste.jpg", "32495544895", address);
            Assert.AreEqual("Rua Itapeva, 164", profile.Address.Address1);
            Assert.AreEqual("Apartamento 145", profile.Address.Address2);
            Assert.AreEqual("01332000", profile.Address.Zipcode);
            Assert.AreEqual("Sao Paulo", profile.Address.City);
            Assert.AreEqual("SP", profile.Address.State);
            Assert.AreEqual("Brazil", profile.Address.Country);
        }
    }
}