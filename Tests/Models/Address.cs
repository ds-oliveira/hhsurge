using NUnit.Framework;
using API.Models;

namespace API.Tests
{
    public class AddressTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void CreateAddress()
        {
            Address address = new Address("Rua Itapeva, 164", "Apartamento 145", "01332000", "Sao Paulo", "SP", "Brazil");
            Assert.AreEqual("Rua Itapeva, 164", address.Address1);
            Assert.AreEqual("Apartamento 145", address.Address2);
            Assert.AreEqual("01332000", address.Zipcode);
            Assert.AreEqual("Sao Paulo", address.City);
            Assert.AreEqual("SP", address.State);
            Assert.AreEqual("Brazil", address.Country);
        }
    }
}