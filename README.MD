# HHSurge - Users API

## Considerações
Primeiramente gostaria de agradecer a oportunidade de participar desse desafio, foi extremamente proveitoso o tempo que dediquei ao projeto. Dado que havia atuado com .NET Core apenas uma vez, tive uma curva de aprendizagem considerável, mas ainda assim espero ter conseguido desenvolver um trabalho satisfatório.

## O Projeto
Como solicitado, o projeto é uma API que tem como objetivo permitir a inserção, alteração, exclusão e consulta de usuários e suas respectivas informações.

Alguns conceitos aplicados:

- Injeção de dependências;
- Documentação viva;
- Paginação;
- Interfaces;
- Métodos estáticos;
- Objetos genéricos;
- Delegação;
- Métodos anônimos;
- Sobrecarga de métodos;
- Sobrecarga de construtores;
- Herança;
- Tratamento de exceções centralizado;
- Enums;
- Centralização de constantes;
- Exceções personalizadas;
- Contratos de requisição;
- Modelos de negócio;
- Injeção de configurações;
- Dockerização.

### Documentação viva
Tomei a liberdade de realizar a implementação do modulo `Swagger` com o objetivo de facilitar os testes da API, tanto em tempo de desenvolvimento quanto por parte do avaliador desse projeto.

Pontos a melhorar:

- Utilizar a documentação XML do Swagger para detalhar o funcionamento dos parâmetros e rotas da aplicação.

### Segurança
O projeto conta com um controle de acesso via API Key. Ainda que da forma como foi implementado não ofereça nenhum ganho real, dado que eu estarei expondo a API em um endereço público, optei por realizar essa implementação.

Pontos a melhorar:

- A API Key encontra-se no `appsettings.json` da aplicação, o que a torna vulneravel;
- Para verbos HTTP onde enviamos os parametros via `body`, a API Key deveria ser enviada no header (optei em implementar dessa forma para facilitar os testes via swagger).

### Logs
Optei por apresentar apenas logs de processamentos relevantes. Nesse caso, os erros foram os escolhidos, sejam eles por parte de falhas na requisição proveniente do client ou por conta de falhas no processamento interno do servidor.

### Banco de dados
Devido a facilidade de hospedagem e gratuidade do servidor de banco de dados oferecido pelo site [cloud.mongodb.com](https://cloud.mongodb.com) optei por utilizar o banco de dados não relacional `mongoDB` como fonte de dados (Obrigado pela sugestão!).

Estrutura do documento (pode ser visto no `Swagger`):
```
{
   "_id":{
      "$oid":"5e654516644c5238e4e8467e"
   },
   "Name":"Cynthia Morais",
   "Email":"cynthiamorais@gmail.com",
   "Profile":{
      "_id":"2f02e517f14d56bed2fa72c4c0671b8a",
      "Type":"personal",
      "Avatar":"https://img2.gratispng.com/20180408/tvw/kisspng-user-computer-icons-gravatar-blog-happy-woman-5aca6d03e6c3f5.6041125615232156199452.jpg",
      "Document":"32493323121",
      "Address":{
         "Address1":"Rua Paranagua, 402",
         "Address2":null,
         "Zipcode":"01332000",
         "City":"Sao Paulo",
         "State":"SP",
         "Country":"Brazil"
      }
   }
}
```

### Testes
A aplicação conta com 39 teste unitários e cobre grande parte das funcionalidades implemetadas.

Pontos a melhorar:

- Implementar o uso de Mock para que os serviços com dependências de repositórios possam ser testados (optei por não implementar devido à relação entre a alta curva de aprendizagem e o tempo disponível).
- Implementar testes de integração.
- Implementar interface de cobertura de testes.

### Versionamento
Optei por utilizar o bitbucket como meu versionador git, pois já possuo conta no mesmo e a plataforma provê buckets privados gratuitos.

### CI/CD
Não implementei devido ao tempo disponível e à ausência da necessidade.

### Entendimento do negócio
Procurei me manter o mais próximo possível dos requisitos informados. No entanto, algumas decisões tiveram de ser tomadas quanto a modelagem, devido ao fato da utilização do `mongoDB`.

- Utilização do id automático gerado pelo `mongoDB` ao cadastrar um novo usuário (id do documento).
- Utilização de um hash MD5 do email do usuário como id do nó `Profile` (acredio que esse id não é necessário, dado a ausência de `joins` devido a utilização do `mongoDB`, ainda assim inclui no documento devido ao requisito inicial).
- Utilização do email do usuário como chave única localizavel (operações de alteração, consulta e exclusão devem informar este dado como fonte para a localização do documento).
- Ao cadastrar um usuário é verificada a unicidade do email informado.
- Uma vez cadastrado, tanto o email, quanto os ids de `Usuario` e `Profile`, não podem ser alterados.
- Todos os campos possuem algum tipo de validação. No entanto, as validações são extremamente burlaveis, dado que os dados de negócio não são exclusivos para um grupo especifico de usúarios, logo eu não tinha o embasamento necessário para saber um tamanho/pattern para os campos (zipcode, por exemplo).

## Rotas da aplicação
A aplicação possui as seguintes rotas:

### GET /Health
Rota de health check da aplicação.

Exemplo de requisição:
```
curl -X GET "http://35.193.224.134/health" -H "accept: */*"
```

Exemplo de resposta (200):
```
{
  "message": "OK"
}
```

### GET /Users
Rota de consulta geral de usuários com suporte à paginação.

Exemplo de requisição:
```
curl -X GET "http://35.193.224.134/Users?apiKey=XMNoQIN80ufaStl7JtWF4BXRgrAfY4zo&limit=10&offset=0" -H "accept: */*"
```

Exemplo de resposta (200):
```
[
  {
    "id": "5e654516644c5238e4e8467e",
    "name": "Cynthia Morais",
    "email": "cynthiamorais@gmail.com",
    "profile": {
      "id": "2f02e517f14d56bed2fa72c4c0671b8a",
      "type": "personal",
      "avatar": "https://img2.gratispng.com/20180408/tvw/kisspng-user-computer-icons-gravatar-blog-happy-woman-5aca6d03e6c3f5.6041125615232156199452.jpg",
      "document": "32493323121",
      "address": {
        "address1": "Rua Paranagua, 402",
        "address2": null,
        "zipcode": "02432000",
        "city": "Sao Paulo",
        "state": "SP",
        "country": "Brazil"
      }
    }
  },
  {
    "id": "5e65b51044130a0001f2828c",
    "name": "Danilo Silva de Oliveira",
    "email": "dsoliveira.developer@gmail.com",
    "profile": {
      "id": "e56c40689eedab160b922e40040a75f4",
      "type": "personal",
      "avatar": "https://img2.gratispng.com/20180408/tvw/kisspng-user-computer-icons-gravatar-blog-happy-woman-5aca6d03e6c3f5.6041125615232156199452.jpg",
      "document": "483051319",
      "address": {
        "address1": "Rua Itapeva, 160",
        "address2": "Apartamento 145",
        "zipcode": "01332000",
        "city": "Sao Paulo",
        "state": "SP",
        "country": "Brazil"
      }
    }
  }
]
```

### GET /User
Rota para consulta de um usuário específico pelo email.

Exemplo de requisição:
```
curl -X GET "http://35.193.224.134/Users?apiKey=XMNoQIN80ufaStl7JtWF4BXRgrAfY4zo&limit=2&offset=0" -H "accept: */*"
```

Exemplo de resposta (200):
```
{
  "id": "5e654516644c5238e4e8467e",
  "name": "Cynthia Morais",
  "email": "cynthiamorais@gmail.com",
  "profile": {
    "id": "2f02e517f14d56bed2fa72c4c0671b8a",
    "type": "personal",
    "avatar": "https://img2.gratispng.com/20180408/tvw/kisspng-user-computer-icons-gravatar-blog-happy-woman-5aca6d03e6c3f5.6041125615232156199452.jpg",
    "document": "32493323121",
    "address": {
      "address1": "Rua Paranagua, 402",
      "address2": null,
      "zipcode": "02432000",
      "city": "Sao Paulo",
      "state": "SP",
      "country": "Brazil"
    }
  }
}
```

### POST /User
Rota para inserção de um novo usuário.

Exemplo de requisição:
```
curl -X POST "http://35.193.224.134/User" -H "accept: */*" -H "Content-Type: application/json" -d "{\"apiKey\":\"XMNoQIN80ufaStl7JtWF4BXRgrAfY4zo\",\"name\":\"Danilo Silva de Oliveira\",\"email\":\"dsoliveira.developer@gmail.com\",\"profile\":{\"type\":\"personal\",\"avatar\":\"https://img2.gratispng.com/20180408/tvw/kisspng-user-computer-icons-gravatar-blog-happy-woman-5aca6d03e6c3f5.6041125615232156199452.jpg\",\"document\":\"483051319\",\"address\":{\"address1\":\"Rua Itapeva, 164\",\"address2\":\"Apartamento 145\",\"zipcode\":\"01332000\",\"city\":\"Sao Paulo\",\"state\":\"SP\",\"country\":\"Brazil\"}}}"
```

Exemplo de resposta (200):
```
{
  "message": "User inserted."
}
```

### PUT /User
Rota para alteração de um usuário existente pelo email.

Exemplo de requisição:
```
curl -X PUT "http://35.193.224.134/User" -H "accept: */*" -H "Content-Type: application/json" -d "{\"apiKey\":\"XMNoQIN80ufaStl7JtWF4BXRgrAfY4zo\",\"name\":\"Danilo Silva de Oliveira\",\"email\":\"dsoliveira.developer@gmail.com\",\"profile\":{\"type\":\"personal\",\"avatar\":\"https://img2.gratispng.com/20180408/tvw/kisspng-user-computer-icons-gravatar-blog-happy-woman-5aca6d03e6c3f5.6041125615232156199452.jpg\",\"document\":\"483051319\",\"address\":{\"address1\":\"Rua Itapeva, 160\",\"address2\":\"Apartamento 145\",\"zipcode\":\"01332000\",\"city\":\"Sao Paulo\",\"state\":\"SP\",\"country\":\"Brazil\"}}}"
```

Exemplo de resposta (200):
```
{
  "message": "User updated."
}
```

### DELETE /User
Rota para exclusão de um usuário pelo email.

Exemplo de requisição:
```
curl -X DELETE "http://35.193.224.134/User" -H "accept: */*" -H "Content-Type: application/json" -d "{\"apiKey\":\"XMNoQIN80ufaStl7JtWF4BXRgrAfY4zo\",\"email\":\"cynthiamorais@gmail.com\"}"
```

Exemplo de resposta (200):
```
{
  "message": "User deleted."
}
```

### Demais respostas possíveis
`400`
```
{
  "message": "There aren't users with the given email."
}
```

`400`
```
{
  "message": "The param 'city' pattern must match the following: '^[A-Za-z\\s]+$'."
}
```

`400`
```
{
  "message": "The profile type is not valid, please choose between 'personal' and 'professional'."
}
```

`400`
```
{
  "message": "A required parameter was not informed: name"
}
```

`400`
```
{
  "message": "The param 'state' size must be between 2 and 50 characters."
}
```

`403`
```
{
  "message": "Invalid API Key."
}
```

`409`
```
{
  "message": "The given email is already registered for another account."
}
```

`500`
```
{
  "message": "Unexpected error."
}
```

## Executando e testando

- .NET CORE SDK: `3.1`

- API KEY: `XMNoQIN80ufaStl7JtWF4BXRgrAfY4zo`

- BASE DE DADOS VAZIA NO MOMENTO DA ENTREGA DO PROJETO.


### Método 1 - Ambiente cloud (Recomendado)
A aplicação esta hospedada no `Kubernetes Engine` no cloud provider `GCP - Google Cloud Platform`, e pode ser acessada através do link a seguir:

http://35.193.224.134/swagger/index.html


### Método 2 - Ambiente docker
Execute o comando a seguir para realizar o download da imagem docker contendo a aplicação:
```
docker run -t -p 80:80 dsoliveiradeveloper/hhsurge:latest
```

Após o comando ser executado com êxito, à aplicação estará disponível na porta 80 de sua máquina:

http://localhost/swagger/index.html


### Método 3 - Ambiente local (Unix)
No diretório raiz do projeto, execute os comandos a seguir:
```
make build
make test
make run
```

Após os comandos serem executados com êxito, à aplicação estará disponível na porta 80 de sua máquina:

http://localhost/swagger/index.html


### Método 4 - Ambiente local
No diretório raiz do projeto, execute os comandos a seguir:
```
dotnet build .
dotnet test Tests
dotnet run -p API
```

Após os comandos serem executados com êxito, à aplicação estará disponível na porta 80 de sua máquina:

http://localhost/swagger/index.html


# Assim chego ao final do meu projeto, tendo dado o meu máximo e tendo aproveitado cada segundo dessa experiência, muito obrigado pela oportunidade e pela atenção!