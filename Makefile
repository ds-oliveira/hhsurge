build:
	dotnet build .

test:
	dotnet test Tests

run:
	dotnet run -p API

publish:
	dotnet publish -c Release -o out