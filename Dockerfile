﻿FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build-env

WORKDIR /app

COPY . .

RUN apt-get update && apt-get install make
RUN make build && make test
RUN make publish

FROM mcr.microsoft.com/dotnet/core/sdk:3.1
WORKDIR /app
COPY --from=build-env /app/out .

CMD ["dotnet", "API.dll"]

EXPOSE 80